import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

import { Album } from '../albums/album.model';
import { AlbumService } from '../albums/album.service';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(private http: HttpClient, private albumService: AlbumService) {}

  storeAlbums() {
    const albums = this.albumService.getAlbums();
    this.http
      .put(
        'https://musicallycrud.firebaseio.com//albums.json',
        albums
      )
      .subscribe(response => {
        console.log(response);
      });
  }

  fetchAlbums() {
    return this.http
      .get<Album[]>(
        'https://musicallycrud.firebaseio.com//albums.json'
      )
      .pipe(
        map(albums => {
          return albums.map(album => {
            return {
              ...album,
              reviews: album.reviews ? album.reviews : []
            };
          });
        }),
        tap(albums => {
          this.albumService.setAlbums(albums);
        })
      );
  }
}
