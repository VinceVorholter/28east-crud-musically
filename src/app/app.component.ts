import { Component, OnInit } from '@angular/core';
import { DataStorageService } from './shared/data-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loadedFeature = 'album';

  constructor(private dataService: DataStorageService) {

  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  ngOnInit() {
    this.dataService.fetchAlbums();
  }
}
