import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Album } from '../../album.model';
import { AlbumService } from '../../album.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-album-review',
  templateUrl: './album-review.component.html',
  styleUrls: ['./album-review.component.scss']
})
export class AlbumReviewComponent implements OnInit {
  reviewForm: FormGroup;
  album: Album;
  id: number;

  constructor(private albumService: AlbumService,
              private route: ActivatedRoute,
              ) {}

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params.id;
          this.album = this.albumService.getAlbum(this.id);
        }
      );

    this.reviewForm = new FormGroup({
      review: new FormControl(null, Validators.required),
      name: new FormControl(null, Validators.required)
    });
  }

  onSubmitReview() {
    this.album.reviews.push(this.reviewForm.value);
    this.albumService.updateAlbum(this.id, this.album);
    this.reviewForm.reset();
  }

}
