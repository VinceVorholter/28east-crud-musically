import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Album } from '../album.model';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss']
})
export class AlbumDetailComponent implements OnInit {
  album: Album;
  id: number;

  constructor(private albumService: AlbumService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params.id;
          this.album = this.albumService.getAlbum(this.id);
        }
      );
  }

  onEditAlbum() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteAlbum() {
    this.albumService.deleteAlbum(this.id);
    this.router.navigate(['/albums']);
  }

}
