import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { AlbumService } from '../album.service';
import { DataStorageService } from 'src/app/shared/data-storage.service';

@Component({
  selector: 'app-album-edit',
  templateUrl: './album-edit.component.html',
  styleUrls: ['./album-edit.component.scss']
})
export class AlbumEditComponent implements OnInit {
  id: number;
  editMode = false;
  albumForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private albumService: AlbumService,
    private router: Router,
    private dataStorageService: DataStorageService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params.id;
      this.editMode = params.id != null;
      this.initForm();
    });
  }

  onSubmit() {
    if (this.editMode) {
      this.albumService.updateAlbum(this.id, this.albumForm.value);
    } else {
      this.albumService.addAlbum(this.albumForm.value);
    }
    this.saveData();
    this.onCancel();
  }

  onDeleteReview(index: number) {
    (this.albumForm.get('reviews') as FormArray).removeAt(index);
    this.saveData();
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  saveData() {
    this.dataStorageService.storeAlbums();
  }

  fetchData() {
    this.dataStorageService.fetchAlbums().subscribe();
  }

  private initForm() {
    let albumName = '';
    let albumArtist = '';
    let albumImagePath = '';
    let albumReviews = new FormArray([]);

    if (this.editMode) {
      const album = this.albumService.getAlbum(this.id);
      albumName = album.name;
      albumArtist = album.artist;
      albumImagePath = album.imagePath;

      if (album.reviews) {
        for (const review of album.reviews) {
          albumReviews.push(
            new FormGroup({
              name: new FormControl(review.name, Validators.required),
              review: new FormControl(review.review, Validators.required)
            })
          );
        }
      }
    }

    this.albumForm = new FormGroup({
      name: new FormControl(albumName, Validators.required),
      artist: new FormControl(albumArtist, Validators.required),
      imagePath: new FormControl(albumImagePath, Validators.required),
      reviews: albumReviews
    });
  }
}
