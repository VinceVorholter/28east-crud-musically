import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Album } from './album.model';
import { Review } from '../shared/review.model';

@Injectable()
export class AlbumService {
  albumsChanged = new Subject<Album[]>();

  private albums: Album[] = [];

  constructor() {}

  setAlbums(albums: Album[]) {
    this.albums = albums;
    this.albumsChanged.next(this.albums.slice());
  }

  getAlbums() {
    return this.albums.slice();
  }

  getAlbum(index: number) {
    return this.albums[index];
  }

  addAlbum(album: Album) {
    this.albums.push(album);
    this.albumsChanged.next(this.albums.slice());
  }

  updateAlbum(index: number, newAlbum: Album) {
    this.albums[index] = newAlbum;
    this.albumsChanged.next(this.albums.slice());
  }

  deleteAlbum(index: number) {
    this.albums.splice(index, 1);
    this.albumsChanged.next(this.albums.slice());
  }
}
