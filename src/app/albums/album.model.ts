import { Review } from '../shared/review.model';

export class Album {
  public name: string;
  public artist: string;
  public description: string;
  public imagePath: string;
  public reviews: Review[];

  constructor(name: string, artist: string, imagePath: string, reviews: Review[]) {
    this.name = name;
    this.artist = artist;
    this.imagePath = imagePath;
    this.reviews = reviews;
  }
}
