import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumsComponent } from './albums/album.component';
import { AlbumStartComponent } from './albums/album-start/album-start.component';
import { AlbumDetailComponent } from './albums/album-detail/album-detail.component';
import { AlbumEditComponent } from './albums/album-edit/album-edit.component';
import { AlbumsResolverService } from './albums/albums.resolver.service';

const appRoutes: Routes = [
  { path: '', redirectTo: '/albums', pathMatch: 'full' },
  {
    path: 'albums',
    component: AlbumsComponent,
    children: [
      { path: '', component: AlbumStartComponent,
        resolve: [AlbumsResolverService] },
      { path: 'new', component: AlbumEditComponent },
      {
        path: ':id',
        component: AlbumDetailComponent,
        resolve: [AlbumsResolverService]
      },
      {
        path: ':id/edit',
        component: AlbumEditComponent,
        resolve: [AlbumsResolverService]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
