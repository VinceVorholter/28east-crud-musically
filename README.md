# Musically CRUD App

This project was made as a technical test.
It's a basic Angular 8 web app with CRUD functions.
It stores it's data in firebase.

## Initital Setup
After cloning navigate to the folder in your terminal.

Run `npm install` to install all dependencies.

Run `npm start` to run a  dev server.

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
